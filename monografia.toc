\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{12}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Objetivo Geral}{12}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Objetivo Espec\IeC {\'\i }fico}{12}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Justificativa}{13}{section.1.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Estado da Arte}}{14}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Modelagem Molecular}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}M\IeC {\'e}todo \textit {ab initeo}}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}An\IeC {\'a}lise Conformacional}{16}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Amino\IeC {\'a}cidos}{18}{section.3.2}
\contentsline {section}{\numberline {3.3}Pept\IeC {\'\i }deos}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Classifica\IeC {\c c}\IeC {\~a}o estrutural}{20}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Algoritmos Evolutivos}{22}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Algoritmos Gen\IeC {\'e}ticos}{22}{subsection.3.4.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{23}{section*.10}
\vspace {\cftbeforechapterskip }

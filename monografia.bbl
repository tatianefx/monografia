\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{v-1.9.6 }

\bibitem[Amino\'acidos 2017]{Aminoacidos}
\abntrefinfo{Amino\'acidos}{AMINO\'ACIDOS}{2017}
{AMINO\'ACIDOS.
In:  WIKIP\'EDIA: a enciclop\'edia livre. Wikimedia, 2017.
Dispon{\'\i}vel em:
  \url{https://pt.wikipedia.org/wiki/Amino\%C3\%A1cido\#cite\_note-ucalgary.27.0-1}.}

\bibitem[Berg, Tymoczko e Stryer 2014]{Berg2014}
\abntrefinfo{Berg, Tymoczko e Stryer}{BERG; TYMOCZKO; STRYER}{2014}
{BERG, J.~M.; TYMOCZKO, J.~L.; STRYER, L. \emph{Bioquímica}. 7. ed. Rio de
  Janeiro: Guanabara Koogan, 2014.
ISBN 978-8-5277-2387-9.}

\bibitem[Berger e Leighton 1998]{Berger1998}
\abntrefinfo{Berger e Leighton}{BERGER; LEIGHTON}{1998}
{BERGER, B.; LEIGHTON, T. Protein folding in the hydrophobic-hydrophilic (hp)
  model is np-complete.
\emph{Journal of Computational Biology}, v.~5, p. 27--40, 1998.}

\bibitem[Carvalho et al. 2003]{Carvalho2003}
\abntrefinfo{Carvalho et al.}{CARVALHO et al.}{2003}
{CARVALHO, I. et al. Introduç\~ao a modelagem molecular de f\'armacos no curso
  experimental de qu\'imica farmac\^eutica.
\emph{Qu\'imica Nova}, scielo, v.~26, p. 428 -- 438, 05 2003.
ISSN 0100-4042.
Dispon{\'\i}vel em:
  \url{http://www.scielo.br/scielo.php?script=sci\_arttext&pid=S0100-40422003000300023&nrm=iso}.}

\bibitem[Crescenzi et al. 1998]{Crescenzi1998}
\abntrefinfo{Crescenzi et al.}{CRESCENZI et al.}{1998}
{CRESCENZI, P. et al. On the complexity of protein folding.
\emph{Journal of Computational Biology}, v.~5, n.~3, p. 423--465, 1998.
Dispon{\'\i}vel em: \url{https://doi.org/10.1089/cmb.1998.5.423}.}

\bibitem[Jensen 2007]{Jensen2007}
\abntrefinfo{Jensen}{JENSEN}{2007}
{JENSEN, F. \emph{Introduction to computational chemistry}. 2. ed. West Sussex
  PO19 8SQ, England: WILEY, 2007.
ISBN 13 978-0-470-01186-7.}

\bibitem[Levine 1991]{Levine1991}
\abntrefinfo{Levine}{LEVINE}{1991}
{LEVINE, I.~N. \emph{Quantum Chemistry}. nglewood Cliffs, New Jersey: Prentice
  Hall: Springer, Dordrecht, 1991.
ISBN 0-205-12770-3.}

\bibitem[Linden 2008]{Ricardo2008}
\abntrefinfo{Linden}{LINDEN}{2008}
{LINDEN, R. \emph{Algoritimos Geneticos: Teoria e Implementação}. 2. ed. Rio
  de Janeiro: BRASFORT, 2008.
ISBN 978-85-7452-373-6.}

\bibitem[Maupetit, Derreumaux e Tuffery 2009]{Maupetit2009}
\abntrefinfo{Maupetit, Derreumaux e Tuffery}{MAUPETIT; DERREUMAUX;
  TUFFERY}{2009}
{MAUPETIT, J.; DERREUMAUX, P.; TUFFERY, P. Pep-fold: an online resource for de
  novo peptide structure prediction.
\emph{Nucleic Acids Research}, v.~37, p. W498--W503, 2009.
Dispon{\'\i}vel em: \url{http://dx.doi.org/10.1093/nar/gks323}.}

\bibitem[M\'etodos ab initio 2017]{MetodosAbinitio2017}
\abntrefinfo{M\'etodos\ldots}{M\'ETODOS\ldots}{2017}
{M\'ETODOS ab initio.
In:  WIKIP\'EDIA: a enciclop\'edia livre. Wikimedia, 2017.
Dispon{\'\i}vel em:
  \url{https://pt.wikipedia.org/wiki/M\%C3\%A9todos\_ab\_initio}.}

\bibitem[Modelagem molecular 2016]{ModelagemMolecular2016}
\abntrefinfo{Modelagem\ldots}{MODELAGEM\ldots}{2016}
{MODELAGEM molecular.
In:  WIKIP\'EDIA: a enciclop\'edia livre. Wikimedia, 2016.
Dispon{\'\i}vel em: \url{https://pt.wikipedia.org/wiki/Modelagem\_molecular}.}

\bibitem[Nelson e Cox 2014]{Nelson2014}
\abntrefinfo{Nelson e Cox}{NELSON; COX}{2014}
{NELSON, D.~L.; COX, M.~M. \emph{Princípios de Bioquímica de Lehninger}. 6.
  ed. Porto Alegre: artmed, 2014.
ISBN 978-85-8271-073-9.}

\bibitem[Ramachandran, Deepa e Namboori 2008]{Ramachandran2008}
\abntrefinfo{Ramachandran, Deepa e Namboori}{RAMACHANDRAN; DEEPA;
  NAMBOORI}{2008}
{RAMACHANDRAN, K.~I.; DEEPA, G.; NAMBOORI, K. \emph{Computational Chemistry and
  Molecular Modeling: Principles and Applications}. Coimbatore, India:
  Springer-Verlag Berlin Heidelberg, 2008.
ISBN 978-3-540-77302-3.}

\bibitem[Rigden 2017]{Rigden2017}
\abntrefinfo{Rigden}{RIGDEN}{2017}
{RIGDEN, D.~J. \emph{From Protein Structure to Function with Bioinformatics}.
  1. ed. United Kingdom: Springer, Dordrecht, 2017.
ISBN 978-1-4020-9057-8.}

\bibitem[Solomons e Fryhle 2000]{Solomons2000}
\abntrefinfo{Solomons e Fryhle}{SOLOMONS; FRYHLE}{2000}
{SOLOMONS, T. W.~G.; FRYHLE, C.~B. \emph{Química Orgânica}. 7. ed. Rio de
  Janeiro: LTC, 2000. v.~1.
ISBN 8521612826.}

\bibitem[Sun 1995]{sun1995genetic}
\abntrefinfo{Sun}{SUN}{1995}
{SUN, S. A genetic algorithm that seeks native states of peptides and proteins.
\emph{Biophysical journal}, Elsevier, v.~69, n.~2, p. 340--355, 1995.}

\bibitem[Th\'evenet et al. 2012]{Thevenet2012}
\abntrefinfo{Th\'evenet et al.}{TH\'EVENET et al.}{2012}
{TH\'EVENET, P. et al. Pep-fold: an updated de novo structure prediction server
  for both linear and disulfide bonded cyclic peptides.
\emph{Nucleic Acids Research}, v.~40, n.~W1, p. W288--W293, 2012.
Dispon{\'\i}vel em: \url{http://dx.doi.org/10.1093/nar/gks419}.}

\end{thebibliography}
